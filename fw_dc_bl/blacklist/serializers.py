from rest_framework import serializers
from blacklist.models import Blacklist


class BlackListSerializer(serializers.ModelSerializer):

    class Meta:
        model = Blacklist
        # List all of the fields that could possibly be included in a request
        # or response, including fields specified explicitly above.
        fields = ('id_webzine', 'hostname', 'ip', 'action',
                  'service', 'comment', 'color', 'count',
                  'begin', 'end')
        read_only_fields = ()

    @property
    def bl_id(self):
        return self._context['view'].kwargs.get("bl_id")
