from django.db import models


class Blacklist(models.Model):
    ip = models.CharField(max_length=50)
    action = models.TextField()
    service = models.TextField()
    begin = models.DateTimeField(blank=True, null=True)
    end = models.DateTimeField(blank=True, null=True)
    comment = models.TextField(blank=True, null=True)
    color = models.CharField(max_length=6, blank=True, null=True)
    hostname = models.TextField(blank=True, null=True)
    id_webzine = models.IntegerField(blank=True, null=True)
    count = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'blacklist'
        unique_together = (('ip', 'action', 'service'),)
