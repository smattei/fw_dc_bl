from blacklist.views import BlacklistViewSet, FilterBlacklistView
from fw_dc_bl.utils import get_doc_view
from rest_framework_extensions.routers import ExtendedSimpleRouter
from django.conf.urls import url

app_name = 'blacklist'

# Entry point for all routes
router = ExtendedSimpleRouter()

# User routes
bl_router = router.register(
    r'blacklist', BlacklistViewSet, base_name='blacklist')


urlpatterns = router.urls

schema_view = get_doc_view(
    title='Firewall BlackList API',
    url='/blacklist/',
    urlconf='blacklist.urls'
)

urlpatterns += [url(r'^(?P<id_webzine>\d+)/blacklist$',
                    FilterBlacklistView.as_view())]
urlpatterns += [url(r'^docs/$', schema_view)]
