from rest_framework.permissions import AllowAny
from rest_framework import viewsets, mixins

from blacklist.models import Blacklist
from blacklist.serializers import BlackListSerializer
from rest_framework.generics import ListAPIView
from rest_framework.generics import GenericAPIView
from fw_dc_bl.filters import WebzineFilterBackend


class BlacklistViewSet(mixins.RetrieveModelMixin, mixins.CreateModelMixin,
                       mixins.UpdateModelMixin, mixins.DestroyModelMixin,
                       viewsets.GenericViewSet):
    serializer_class = BlackListSerializer
    # authentication_classes = (RegularAuthentication, )
    # permission_classes = (IsAuthenticated,)
    permission_classes = (AllowAny,)
    queryset = Blacklist.objects.all()


class FilterBlacklistView(ListAPIView, GenericAPIView):
    queryset = Blacklist.objects.all()
    serializer_class = BlackListSerializer
    permission_classes = (AllowAny,)
    filter_backends = (WebzineFilterBackend, )
