import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


class DefaultConfig:

    DATABASE_NAME = 'firewall'
    DATABASE_USER = 'firewall'
    DATABASE_PASSWORD = 'firewall'
    DATABASE_HOST = '172.18.0.1'
    DATABASE_PORT = '5435'
    DATABASE_ENGINE = 'django.db.backends.postgresql_psycopg2'

    TEST_ENVIRONMENT = True

    # DATABASE_ENGINE = 'django.db.backends.sqlite3'
    # DATABASE_NAME = os.path.join(BASE_DIR, 'db.sqlite3')


class Config(DefaultConfig):
    IS_DEFAULT = True
    DEBUG = True
