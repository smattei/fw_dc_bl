from rest_framework import filters
from django.db.models import Func


class NullIf(Func):
    template = "NULLIF(%(expressions)s, '')"


class WebzineFilterBackend(filters.BaseFilterBackend):
    """
    Filter that restricts visible DB objects to the current webzine_id.
    """

    filtered_field = 'id_webzine'

    def filter_queryset(self, request, queryset, view):

        view.id_webzine = view.kwargs.get('id_webzine')
        return queryset.filter(
            **{self.filtered_field: view.id_webzine})
