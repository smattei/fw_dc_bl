#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
. $DIR/const.sh

for i in $BASE; do mkdir -p $i; chown -R www-data $i; done

$UWSGI --socket $SOCKET --chmod-socket=666 --chdir $ROOT --stats :$PORT --stats-http --master -w $WSGIRELPATH.wsgi:application --processes 20 --enable-threads --listen 128 --harakiri 300 --pidfile $PIDFILE --logto $LOGFILE

