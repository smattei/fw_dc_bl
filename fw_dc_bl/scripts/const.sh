#!/bin/bash
BASE="/var/run/fw_dc_bl/ /var/log/fw_dc_bl/"
PIDFILE="/var/run/fw_dc_bl/fw_dc_bl.pid"
SOCKET="/var/run/fw_dc_bl/fw_dc_bl.sock"
LOGFILE="/var/log/fw_dc_bl/uwsgi.log"
ROOT="/opt/fw_dc_bl"
PORT="666"
UWSGI="uwsgi"
WSGIRELPATH="fw_dc_bl"


chmod +rwx ./scripts/collectstatic.sh
./scripts/collectstatic.sh