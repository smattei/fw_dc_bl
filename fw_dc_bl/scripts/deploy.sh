#!/bin/bash

cd /opt/fw_dc_bl/fw_dc_bl/

git pull origin master && gcloud docker -- pull eu.gcr.io/goodbarber-prod/fw_dc_bl:latest && docker-compose up -d
