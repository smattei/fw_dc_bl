#!/bin/bash

cd /opt/fw_dc_bl/fw_dc_bl

GIT_COMMIT=`git rev-parse --short HEAD`


docker build --build-arg CONFIG=prod -t eu.gcr.io/goodbarber-prod/fw_dc_bl .
gcloud docker -- push eu.gcr.io/goodbarber-prod/fw_dc_bl
